# coding: utf-8
import os
import logging
from baidubce.bce_client_configuration import BceClientConfiguration
from baidubce.auth.bce_credentials import BceCredentials
from baidubce.services.bos.bos_client import BosClient

BOS_HOST = os.environ['BOS_HOST'] if 'BOS_HOST' in os.environ else 'http://su.bcebos.com'
BOS_AK = os.environ['BOS_AK'] if 'BOS_AK' in os.environ else '0ee68e81c7f94b2685ce0a2a1d9705e6'
BOS_SK = os.environ['BOS_SK'] if 'BOS_SK' in os.environ else '132994492265449fa53b16a7ef73d39a'

BOS_BUCKET = os.environ['BOS_BUCKET'] if 'BOS_BUCKET' in os.environ else '58fd-test'
BOS_IA = eval(os.environ['BOS_IA']) if 'BOS_IA' in os.environ else False

MYSQL_URL = os.environ['MYSQL_URL'] if 'MYSQL_URL' in os.environ else None

REDIS_HOST = 'redis'
REDIS_PORT = 6379
REDIS_PARAMS = None


IMAGES_THUMBS = eval(os.environ['IMAGES_THUMBS']) if 'IMAGES_THUMBS' in os.environ else {'80_80': (80, 80),
                                                                                         '400_400': (400, 400)}

IMAGES_STORE = os.path.abspath(os.path.dirname(__file__))
logger = logging.getLogger('baidubce.services.bos.bosclient')

IMAGES_LOCAL_SAVE = eval(os.environ['IMAGES_LOCAL_SAVE']) if 'IMAGES_LOCAL_SAVE' in os.environ else False
fh = logging.FileHandler(os.path.join(IMAGES_STORE, 'log/wiki.log'))
fh.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.setLevel(logging.ERROR)
logger.addHandler(fh)

config = BceClientConfiguration(credentials=BceCredentials(BOS_AK, BOS_SK), endpoint=BOS_HOST)

bos_client = BosClient(config)
