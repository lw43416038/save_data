# coding: utf-8
from sqlalchemy import Column, Date, Integer, String, Text,create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from settings import MYSQL_URL

Base = declarative_base()



class TestUlb(Base):
    __tablename__ = 'ulb_total'

    id = Column(Integer, primary_key=True)
    bClassify = Column(String(25))
    sClassify = Column(String(25))
    goodsName = Column(String(255))
    ysprice = Column(String(25))
    dhprice = Column(String(25))
    goodsNum = Column(String(50))
    corPattern_src = Column(String(500))
    corPattern_name = Column(String(50))
    shelf_time = Column(Date)
    deliver_time = Column(String(50))
    freight = Column(String(50))
    prodPlace = Column(String(50))
    style = Column(String(255))
    crowd = Column(String(50))
    season = Column(String(50))
    classify = Column(String(50))
    goodsDesc = Column(Text)
    sense_img = Column(String(512))
    sense_thickness = Column(String(20))
    sense_elasticity = Column(String(20))
    sense_compliance = Column(String(20))
    sense_permeability = Column(String(20))
    sameType = Column(Text)
    shopId = Column(Integer, nullable=False)


class UlbShangjia(Base):
    __tablename__ = 'ulb_shangjia'

    id = Column(Integer, primary_key=True)
    shopkeeper = Column(String(255))
    phone = Column(String(50))
    shopName = Column(String(128))
    scope = Column(String(255))

engine = create_engine(MYSQL_URL)
Base.metadata.create_all(engine)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)