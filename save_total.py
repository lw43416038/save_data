#! /usr/bin/env python
# -*- coding: utf-8 -*-
from redis import Redis
import json
from models import Session, TestUlb, UlbShangjia
from settings import logger, REDIS_HOST, REDIS_PORT,REDIS_PARAMS
from uploadBos import UploadImages
from threading import Thread

r = Redis(host=REDIS_HOST, port=REDIS_PORT, password=REDIS_PARAMS)
upload_image = UploadImages()


def run():
    session = Session()
    while True:
        src_data = r.brpop('ulb:total')[1].decode('utf8')
        print src_data
        data = json.loads(src_data)
        good_urls = []
        for i in data['goodsDesc']:
            result = upload_image.get_image(i)
            if result:
                good_urls.append(result[0])
        data['goodsDesc'] = json.dumps(good_urls, ensure_ascii=False)

        data['sameType'] = json.dumps(data['sameType'], ensure_ascii=False)

        if data['sense_img']:
            result1 = upload_image.get_image(data['sense_img'])
            if result1:
                data['sense_img'] = result1[0]
        if data['corPattern_src']:
            result2 = upload_image.get_image(data['corPattern_src'])
            if result2:
                data['corPattern_src'] = result2[0]

        try:
            obj = session.query(UlbShangjia).filter_by(shopkeeper=data['shopkeeper'], phone=data['phone']).first()
            if not obj:
                session.bulk_insert_mappings(UlbShangjia, [data])
                session.commit()
                obj = session.query(UlbShangjia).filter_by(shopkeeper=data['shopkeeper'], phone=data['phone']).first()
                data['shopId'] = obj.id
            else:
                data['shopId'] = obj.id
            session.bulk_insert_mappings(TestUlb, [data])
            session.commit()
        except Exception as e:
            logger.exception(e)
            session.rollback()


if __name__ == '__main__':
    t_list = []
    for i in xrange(30):
        t = Thread(target=run)
        t.start()
        t_list.append(t)
    for j in t_list:
        j.join()
