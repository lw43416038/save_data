# coding=utf-8
import os
import time
import random
import settings
import requests
import hashlib
from settings import bos_client, logger
from retry import retry
from baidubce.services.bos import storage_class
from redis import StrictRedis
from requests.exceptions import ProxyError, ConnectionError
from useragent import agents
from PIL import Image

redis_db = StrictRedis(
    host='data.cunt.group',
    port=6379,
    password='SENCHUANshujuredis',
    db=9,
)


def checkProxy(proxy):
    """
    测试代理是否可用
    :param proxy:
    :return:
    """
    url = "http://xxxx"
    proxies = {
        "http": proxy,
        "https": proxy
    }
    flag = True
    try:
        requests.get(url, proxies=proxies, timeout=0.1)
    except ProxyError:
        flag = False
    except ConnectionError:
        flag = False
    except:
        pass
    return flag


@retry(Exception, tries=10, logger=logger)
def img_request(url):
    # 使用代理ip池的中间件
    # 配合https://github.com/arthurmmm/hq-proxies使用
    # 若使用请在settings中开启

    while True:
        proxy = redis_db.srandmember("hq-proxies:proxy_pool", 1)
        proxy = proxy[0].decode()
        flag = checkProxy(proxy)
        if flag:
            break

    agent = random.choice(agents)

    headers = {
        'User-Agent': agent
    }
    proxies = {
        'http': proxy,
        'https': proxy,
    }
    resp = requests.get(url=url, headers=headers, proxies=proxies, timeout=10)

    if resp.status_code == 200:
        return resp
    else:
        raise ProxyError


def convert_image(image_path, out_path, size=None):
    image = Image.open(image_path)
    if image.format == 'PNG' and image.mode == 'RGBA':
        background = Image.new('RGBA', image.size, (255, 255, 255))
        background.paste(image, image)
        image = background.convert('RGB')
    elif image.mode == 'P':
        image = image.convert("RGBA")
        background = Image.new('RGBA', image.size, (255, 255, 255))
        background.paste(image, image)
        image = background.convert('RGB')
    elif image.mode != 'RGB':
        image = image.convert('RGB')

    if size:
        image = image.copy()
        image.thumbnail(size, Image.ANTIALIAS)

    # buf = BytesIO()
    image.save(out_path, 'JPEG')
    return True


def md5sum(file_path):
    """Calculate the md5 checksum of a file-like object without reading its
    whole content in memory.

    # >>> from io import BytesIO
    # >>> md5sum(BytesIO(b'file content to hash'))
    '784406af91dd5a54fbb9c84c2236595a'
    """
    with open(file_path, 'rb') as file:
        m = hashlib.md5()
        while True:
            d = file.read(8096)
            if not d:
                break
            m.update(d)
        return m.hexdigest()


@retry(Exception, tries=4, logger=logger)
def bos_upload(filename, bucket, object, IA=False):
    if IA:
        storage = storage_class.STANDARD_IA
    else:
        storage = storage_class.STANDARD
    bos_client.put_object_from_file(bucket, object, filename, storage_class=storage)
    return True


class UploadImages(object):
    def __init__(self):
        pass

    def get_image(self, url):
        date_now = time.strftime("%Y%m%d", time.localtime(time.time()))
        date_path = os.path.join(settings.IMAGES_STORE, 'images/ml/{}'.format(date_now))
        if not os.path.exists(date_path):
            os.system('mkdir -p {}'.format(date_path))
        img_name = url.replace('/', '').replace('.', '').replace(':', '').replace(' ', '')
        path = 'images/ml/{}/{}'.format(date_now, img_name)
        img_path = os.path.join(settings.IMAGES_STORE, path)
        try:
            resp = img_request(url)
            with open(img_path, 'wb') as img:
                img.write(resp.content)

            if not settings.IMAGES_THUMBS:
                images_thumbs = {}
            else:
                images_thumbs = settings.IMAGES_THUMBS
            for p, size in images_thumbs.items():
                thumb_path = 'images/thumbs/{}/{}/{}'.format(p, date_now, img_name)
                thumb_data_path = 'images/thumbs/{}/{}'.format(p, date_now)
                if not os.path.exists(thumb_data_path):
                    os.system('mkdir -p {}'.format(thumb_data_path))
                out_path = os.path.join(settings.IMAGES_STORE, thumb_path)
                convert_image(img_path, out_path, size)

            result = self.upload_img(url, img_path, img_name, path)
            for p, size in images_thumbs.items():
                thumb_path = 'images/thumbs/{}/{}/{}'.format(p, date_now, img_name)
                thumb_path = os.path.join(settings.IMAGES_STORE, thumb_path)
                new_thumb_path = 'images/thumbs/{}/{}'.format(p, result[0])
                new_thumb_path = os.path.join(settings.IMAGES_STORE, new_thumb_path)
                os.rename(thumb_path, new_thumb_path)
            return result
        except Exception as e:
            logger.error('image not found {}: {}'.format(e, url))
            return False

    def upload_img(self, url, img_path, img_name, path):
        md5_hash = md5sum(img_path)
        new_path = img_path.split('/')
        new_path[-1] = md5_hash + '.jpg'
        new_path = '/'.join(new_path)
        os.rename(img_path, new_path)
        object_key = '/'.join(new_path.split('/')[-2:])
        if bos_upload(new_path, settings.BOS_BUCKET, "ml/" + object_key, IA=settings.BOS_IA):
            logger.info("success upload to bce: {}".format(url))
            if not settings.IMAGES_LOCAL_SAVE:
                os.remove(new_path)
            return object_key, md5_hash
        logger.error("fail upload to bce{}: {}".format(new_path, url))
        return False
